package rtretyak.tlgrm.domain.pubg;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty
import rtretyak.tlgrm.domain.pubg.stats.LiveTracking
import rtretyak.tlgrm.domain.pubg.stats.Match;

public class PubgPlayer {
    @JsonProperty("AccountId")
    String accountId;
    @JsonProperty("Nickname")
    String nickname;
    @JsonProperty("AvatarUrl")
    String avatarUrl;
    @JsonProperty("SteamId")
    String steamId;
    @JsonProperty("SteamName")
    String steamName;
    @JsonProperty("State")
    String state;
    @JsonProperty("InviteAllow")
    String inviteAllow;

    Match latestMatch
    LiveTracking latestLiveTracking

    @JsonIgnore
    Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
