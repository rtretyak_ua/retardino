package rtretyak.tlgrm.domain.pubg.stats

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Stats {

    @JsonProperty("platformId")
    public Integer platformId;
    @JsonProperty("AccountId")
    public String accountId;
    @JsonProperty("Avatar")
    public String avatar;
    @JsonProperty("selectedRegion")
    public String selectedRegion;
    @JsonProperty("defaultSeason")
    public String defaultSeason;
    @JsonProperty("seasonDisplay")
    public String seasonDisplay;
    @JsonProperty("LastUpdated")
    public String lastUpdated;
    @JsonProperty("LiveTracking")
    public List<LiveTracking> liveTracking = null;
    @JsonProperty("PlayerName")
    public String playerName;
    @JsonProperty("PubgTrackerId")
    public Integer pubgTrackerId;
    @JsonProperty("Stats")
    public List<Stat> stats = null;
    @JsonProperty("MatchHistory")
    public List<Match> matchHistory;
    @JsonIgnore
    Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
