package rtretyak.tlgrm.domain.pubg.stats

import com.fasterxml.jackson.annotation.JsonAnyGetter
import com.fasterxml.jackson.annotation.JsonAnySetter
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Created by tretyak on 7/19/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
class Match {
    @JsonProperty("Id")
    public Integer id;
    @JsonProperty("Updated")
    public String updated;
    @JsonProperty("UpdatedJS")
    public String updatedJS;
    @JsonProperty("Season")
    public Integer season;
    @JsonProperty("SeasonDisplay")
    public String seasonDisplay;
    @JsonProperty("Match")
    public Integer match;
    @JsonProperty("MatchDisplay")
    public String matchDisplay;
    @JsonProperty("Region")
    public Integer region;
    @JsonProperty("RegionDisplay")
    public String regionDisplay;
    @JsonProperty("Rounds")
    public Integer rounds;
    @JsonProperty("Wins")
    public Integer wins;
    @JsonProperty("Kills")
    public Integer kills;
    @JsonProperty("Assists")
    public Integer assists;
    @JsonProperty("Top10")
    public Integer top10;
    @JsonProperty("Rating")
    public Double rating;
    @JsonProperty("RatingChange")
    public Double ratingChange;
    @JsonProperty("RatingRank")
    public Integer ratingRank;
    @JsonProperty("RatingRankChange")
    public Integer ratingRankChange;
    @JsonProperty("Headshots")
    public Integer headshots;
    @JsonProperty("Kd")
    public Double kd;
    @JsonProperty("Damage")
    public Integer damage;
    @JsonProperty("TimeSurvived")
    public Double timeSurvived;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
