package rtretyak.tlgrm.domain.pubg.stats

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Stat {

    @JsonProperty("Region")
    public String region;
    @JsonProperty("Season")
    public String season;
    @JsonProperty("Match")
    public String match;
    @JsonProperty("Stats")
    public List<RegionStat> stats = null;
    @JsonIgnore
    Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
