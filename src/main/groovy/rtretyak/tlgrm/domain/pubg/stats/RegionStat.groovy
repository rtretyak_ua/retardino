package rtretyak.tlgrm.domain.pubg.stats;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RegionStat {

    @JsonProperty("partition")
    public Object partition;
    @JsonProperty("label")
    public String label;
    @JsonProperty("subLabel")
    public Object subLabel;
    @JsonProperty("field")
    public String field;
    @JsonProperty("category")
    public String category;
    @JsonProperty("ValueInt")
    public Object valueInt;
    @JsonProperty("ValueDec")
    public Double valueDec;
    @JsonProperty("value")
    public String value;
    @JsonProperty("rank")
    public Integer rank;
    @JsonProperty("percentile")
    public Integer percentile;
    @JsonProperty("displayValue")
    public String displayValue;
    @JsonIgnore
    Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
