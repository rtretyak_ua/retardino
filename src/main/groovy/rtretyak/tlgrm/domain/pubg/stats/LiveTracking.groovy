package rtretyak.tlgrm.domain.pubg.stats;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty
import groovy.transform.Canonical;

@Canonical
public class LiveTracking {

    @JsonProperty("Match")
    public Integer match;
    @JsonProperty("MatchDisplay")
    public String matchDisplay;
    @JsonProperty("Season")
    public Integer season;
    @JsonProperty("RegionId")
    public Integer regionId;
    @JsonProperty("Region")
    public String region;
    @JsonProperty("Date")
    public String date;
    @JsonProperty("Delta")
    public Double delta;
    @JsonProperty("Value")
    public Double value;
    @JsonProperty("message")
    public String message;
    @JsonIgnore
    Map<String, Object> additionalProperties = new HashMap<String, Object>()

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
