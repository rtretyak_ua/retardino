package rtretyak.tlgrm.domain.dota

import rtretyak.tlgrm.domain.dota.matchdetails.MatchDetails;

/**
 * Created by PC on 22.05.2017.
 */
public class DotaPlayer extends Player {
    Long latestGameDate = 0L;
    MatchDetails latestMatchDetails
    Integer currentStreak = 0;

    def updateStreak(boolean hasWon) {
        if(hasWon) {
            currentStreak = currentStreak >= 0 ? currentStreak + 1 : 1
        }
        else {
            currentStreak = currentStreak <= 0 ? currentStreak - 1 : -1
        }
    }
}
