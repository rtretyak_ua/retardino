package rtretyak.tlgrm.domain.dota.matchdetails;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MatchDetailsPlayer {

    @JsonProperty("account_id")
    public Long accountId;
    @JsonProperty("player_slot")
    public Integer playerSlot;
    @JsonProperty("hero_id")
    public Integer heroId;
    @JsonProperty("item_0")
    public Integer item0;
    @JsonProperty("item_1")
    public Integer item1;
    @JsonProperty("item_2")
    public Integer item2;
    @JsonProperty("item_3")
    public Integer item3;
    @JsonProperty("item_4")
    public Integer item4;
    @JsonProperty("item_5")
    public Integer item5;
    @JsonProperty("backpack_0")
    public Integer backpack0;
    @JsonProperty("backpack_1")
    public Integer backpack1;
    @JsonProperty("backpack_2")
    public Integer backpack2;
    @JsonProperty("kills")
    public Integer kills;
    @JsonProperty("deaths")
    public Integer deaths;
    @JsonProperty("assists")
    public Integer assists;
    @JsonProperty("leaver_status")
    public Integer leaverStatus;
    @JsonProperty("last_hits")
    public Integer lastHits;
    @JsonProperty("denies")
    public Integer denies;
    @JsonProperty("gold_per_min")
    public Integer goldPerMin;
    @JsonProperty("xp_per_min")
    public Integer xpPerMin;
    @JsonProperty("level")
    public Integer level;
    @JsonProperty("hero_damage")
    public Integer heroDamage;
    @JsonProperty("tower_damage")
    public Integer towerDamage;
    @JsonProperty("hero_healing")
    public Integer heroHealing;
    @JsonProperty("gold")
    public Integer gold;
    @JsonProperty("gold_spent")
    public Integer goldSpent;
    @JsonProperty("scaled_hero_damage")
    public Integer scaledHeroDamage;
    @JsonProperty("scaled_tower_damage")
    public Integer scaledTowerDamage;
    @JsonProperty("scaled_hero_healing")
    public Integer scaledHeroHealing;
    @JsonProperty("ability_upgrades")
    public List<AbilityUpgrade> abilityUpgrades = null;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
