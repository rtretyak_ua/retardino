package rtretyak.tlgrm.domain.dota.matchdetails

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonValue

/**
 * Created by tretyak on 6/30/2017.
 */
enum LobbyType {
    Public_matchmaking(0),
    Practice(1),
    Tournament(2),
    Tutorial(3),
    Coop_with_AI(4),
    Team_match(5),
    Solo_queue(6),
    Ranked_matchmaking(7),
    Solo_Mid_1_vs_1(8),
    Invalid(-1);

    private final int value;

    private LobbyType(int value) {
        this.value = value;
    }
    @JsonValue
    public int getValue() {
        return value;
    }

    @JsonCreator
    public static LobbyType fromValue(int typeCode) {
        for (LobbyType c: LobbyType.values()) {
            if (c.value == typeCode) {
                return c;
            }
        }
        return LobbyType.Invalid
    }
}