package rtretyak.tlgrm.domain.dota.matchdetails

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonValue

/**
 * Created by tretyak on 6/30/2017.
 */
enum GameMode {
    All_Pick(1),
    Captains_Mode(2),
    Random_Draft(3),
    Single_Draft(4),
    All_Random(5),
    INTRO_DEATH(6),
    The_Diretide(7),
    Reverse_Captains_Mode(8),
    Greeviling(9),
    Tutorial(10),
    Mid_Only(11),
    Least_Played(12),
    New_PlayerPool(13),
    Compendium_Matchmaking(14),
    Custom(15),
    Captains_Draft(16),
    Balanced_Draft(17),
    Ability_Draft(18),
    Event(19),
    All_Random_Death_Match(20),
    Solo_Mid_1_vs_1(21),
    Ranked_All_Pick(22),
    Invalid(-1)

    private final int value;

    private GameMode(int value) {
        this.value = value;
    }

    @JsonValue
    public int getValue() {
        return value;
    }

    @JsonCreator
    public static GameMode fromValue(int typeCode) {
        for (GameMode c: GameMode.values()) {
            if (c.value == typeCode) {
                return c;
            }
        }
        return GameMode.Invalid

    }
}