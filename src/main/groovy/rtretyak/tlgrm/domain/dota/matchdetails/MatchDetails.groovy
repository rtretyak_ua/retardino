package rtretyak.tlgrm.domain.dota.matchdetails;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MatchDetails {

    public List<MatchDetailsPlayer> players = null;
    @JsonProperty("radiant_win")
    public Boolean radiantWin;
    public Integer duration;
    @JsonProperty("pre_game_duration")
    public Integer preGameDuration;
    @JsonProperty("start_time")
    public Integer startTime;
    @JsonProperty("match_id")
    public Integer matchId;
    @JsonProperty("match_seq_num")
    public Integer matchSeqNum;
    @JsonProperty("tower_status_radiant")
    public Integer towerStatusRadiant;
    @JsonProperty("tower_status_dire")
    public Integer towerStatusDire;
    @JsonProperty("barracks_status_radiant")
    public Integer barracksStatusRadiant;
    @JsonProperty("barracks_status_dire")
    public Integer barracksStatusDire;
    @JsonProperty("cluster")
    public Integer cluster;
    @JsonProperty("first_blood_time")
    public Integer firstBloodTime;
    @JsonProperty("lobby_type")
    public LobbyType lobbyType;
    @JsonProperty("human_players")
    public Integer humanPlayers;
    @JsonProperty("leagueid")
    public Integer leagueid;
    @JsonProperty("positive_votes")
    public Integer positiveVotes;
    @JsonProperty("negative_votes")
    public Integer negativeVotes;
    @JsonProperty("game_mode")
    public GameMode gameMode;
    @JsonProperty("flags")
    public Integer flags;
    @JsonProperty("engine")
    public Integer engine;
    @JsonProperty("radiant_score")
    public Integer radiantScore;
    @JsonProperty("dire_score")
    public Integer direScore;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
