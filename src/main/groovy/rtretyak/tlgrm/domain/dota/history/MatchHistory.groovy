package rtretyak.tlgrm.domain.dota.history

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty

public class MatchHistory {

    Integer status
    @JsonProperty("num_results")
    Integer numResults;
    @JsonProperty("total_results")
    Integer totalResults;
    @JsonProperty("results_remaining")
    Integer resultsRemaining;
    List<Match> matches = null

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
