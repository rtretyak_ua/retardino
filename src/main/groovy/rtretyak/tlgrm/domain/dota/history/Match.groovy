package rtretyak.tlgrm.domain.dota.history;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty
import rtretyak.tlgrm.domain.dota.matchdetails.LobbyType;


public class Match {

    @JsonProperty("match_id")
    Long matchId;
    @JsonProperty("match_seq_num")
    Integer matchSeqNum;
    @JsonProperty("start_time")
    Integer startTime;
    @JsonProperty("lobby_type")
    LobbyType lobbyType;
    @JsonProperty("radiant_team_id")
    Integer radiantTeamId;
    @JsonProperty("dire_team_id")
    Integer direTeamId;
    @JsonProperty("players")
    List<MatchHistoryPlayer> players = null;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
