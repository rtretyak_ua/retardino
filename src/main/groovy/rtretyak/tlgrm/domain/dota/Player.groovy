package rtretyak.tlgrm.domain.dota;

import com.fasterxml.jackson.annotation.*;

public class Player {

    @JsonProperty("steamid")
    String steamid;
    @JsonProperty("communityvisibilitystate")
    Integer communityvisibilitystate;
    @JsonProperty("profilestate")
    Integer profilestate;
    @JsonProperty("personaname")
    String personaname;
    @JsonProperty("lastlogoff")
    Integer lastlogoff;
    @JsonProperty("profileurl")
    String profileurl;
    @JsonProperty("avatar")
    String avatar;
    @JsonProperty("avatarmedium")
    String avatarmedium;
    @JsonProperty("avatarfull")
    String avatarfull;
    @JsonProperty("personastate")
    Integer personastate;
    @JsonProperty("realname")
    String realname;
    @JsonProperty("primaryclanid")
    String primaryclanid;
    @JsonProperty("timecreated")
    Integer timecreated;
    @JsonProperty("personastateflags")
    Integer personastateflags;
    @JsonProperty("gameextrainfo")
    String gameextrainfo;
    @JsonProperty("gameid")
    String gameid;
    @JsonIgnore
    Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
