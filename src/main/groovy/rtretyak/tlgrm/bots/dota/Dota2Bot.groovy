package rtretyak.tlgrm.bots.dota

import groovyx.net.http.RESTClient
import org.apache.log4j.Logger
import rtretyak.tlgrm.Main
import rtretyak.tlgrm.bots.Bot
import rtretyak.tlgrm.domain.dota.DotaPlayer
import rtretyak.tlgrm.domain.dota.Hero
import rtretyak.tlgrm.domain.dota.history.MatchHistory
import rtretyak.tlgrm.domain.dota.matchdetails.MatchDetailsPlayer

import static rtretyak.tlgrm.domain.dota.matchdetails.LobbyType.*
import rtretyak.tlgrm.domain.dota.matchdetails.MatchDetails

/**
 * Created by tretyak on 6/29/2017.
 */
class Dota2Bot extends Bot {
    private final Logger LOGGER = Logger.getLogger(this.class);

    private String steamKey;
    private List<Long> playerIds;
    private Hero[] heroes = initHeroes();
    private Random r = new Random();
    RESTClient steamApi
    private static final String STEAM_API_URL = "https://api.steampowered.com"
    private static final String MATCH_HISTORY_URL = "/IDOTA2Match_570/GetMatchHistory/V001/";
    private static final String MATCH_DETAILS_URL = "/IDOTA2Match_570/GetMatchDetails/V001/";
    private static final String playersApi = "/ISteamUser/GetPlayerSummaries/v2/";
    private static List<String> winPhrases = ["затащил", "забожил",
                                              "порешал все вопросы", "был молодцом", "вытащил за счет союзников", "улыбнулась удача",
                                              "АЙ МОЛОДЭЦЬ"]
    private static List<String> losePhrases = ["отсосал", "насосал",
                                               "проебал", "был плох", "потянул команду на дно", "потерпел позорное поражение",
                                               "О НЕТ МОИ ПЭТЭЭС"]
    def allowedModes = [Solo_queue, Team_match, Ranked_matchmaking]

    public Dota2Bot(String token, String steamKey, List<Long> recipients, List<Long> playerIds) {
        super(token, recipients);
        this.steamKey = steamKey;
        this.playerIds = playerIds;
        steamApi = new RESTClient(STEAM_API_URL)
    }

    @Override
    public void start() throws Exception {
        List<DotaPlayer> players = initPlayers();
        sendMessage("Coming online.");

        for (DotaPlayer player : players) {
            MatchHistory history = steamApi.get(path: MATCH_HISTORY_URL,
                    query: [key: steamKey, account_id: player.steamid]).data.result.to(MatchHistory)
            player.currentStreak = calcCurrentStreak(player, history)
            player.latestGameDate = history.matches.find {
                it.lobbyType in allowedModes
            }?.getStartTime()?.longValue()?.multiply(1000)
        }
        while (true) {
            try {
                pingSteamForUpdates(players);
            } catch (Throwable t) {
                LOGGER.error("Failed to ping for updates", t)
                Thread.sleep(30000);
                LOGGER.info("Sleeping for 30s..");
            }
        }
    }

    private Integer calcCurrentStreak(DotaPlayer player, MatchHistory history) {
        def streak = 0
        for (match in history.matches.findAll { it.lobbyType in allowedModes }) {
            MatchDetails deets = steamApi.get(path: MATCH_DETAILS_URL,
                    query: [key: steamKey, match_id: match.getMatchId()]).data.result.to(MatchDetails)
            MatchDetailsPlayer matchPlayer = deets.players.find {
                it.accountId == convertCommunityIdToSteamId3(Long.valueOf(player.steamid))
            }
            boolean won = !(matchPlayer.playerSlot < 99 ^ deets.radiantWin)
            if (won) {
                if (streak < 0) {
                    break;
                }
                streak++
            } else {
                if (streak > 0) {
                    break;
                }
                streak--
            }
        }
        return streak
    }

    private void pingSteamForUpdates(List<DotaPlayer> players) throws InterruptedException {
        for (DotaPlayer player : players) {
            MatchHistory history = steamApi.get(path: MATCH_HISTORY_URL,
                    query: [key: steamKey, account_id: player.steamid]).data.result.to(MatchHistory)
            def latestMatch = history.matches.find { it.lobbyType in allowedModes }
            if (latestMatch == null) {
                LOGGER.debug("No match found for player $player.personaname")
                continue
            }
            long startTime = latestMatch.getStartTime().longValue() * 1000
            MatchDetails deets = steamApi.get(path: MATCH_DETAILS_URL,
                    query: [key: steamKey, match_id: latestMatch.getMatchId()]).data.result.to(MatchDetails)
            if (startTime > player.getLatestGameDate()) {
                player.setLatestGameDate(startTime);
                player.latestMatchDetails = deets
                sendMessage(getLatestMatchStatsMessage(player));
            }
        }
        LOGGER.info("Sleeping for 20s..");
        Thread.sleep(20000);
    }

    private String getLatestMatchStatsMessage(DotaPlayer player) {
        MatchDetailsPlayer matchPlayer = player.latestMatchDetails.players.find {
            it.accountId == convertCommunityIdToSteamId3(Long.valueOf(player.steamid))
        }
        String heroName = heroes.find { it.id == matchPlayer.heroId }.localized_name
        boolean hasWon = !(matchPlayer.playerSlot < 99 ^ player.latestMatchDetails.radiantWin)
        player.updateStreak(hasWon)
        return String.format("%s %s на %s %s-%s-%s, streak: %s", player.personaname,
                hasWon ? winPhrases.get(r.nextInt(winPhrases.size())) : losePhrases.get(r.nextInt(losePhrases.size())),
                heroName, matchPlayer.kills, matchPlayer.deaths, matchPlayer.assists, player.currentStreak);
    }

    private List<DotaPlayer> initPlayers() throws Exception {
        def playersResponse = steamApi.get(path: playersApi,
                query: [key: steamKey, steamids: String.join(",", playerIds*.toString())]).data.response.players
        return mapper.convertValue(playersResponse, DotaPlayer[]).toList()
    }

    private Hero[] initHeroes() {
        ClassLoader classLoader = new Main().getClass().getClassLoader();
        try {
            return mapper.readValue(classLoader.getResource("heroes.json"), Hero[].class);
        } catch (IOException e) {
            LOGGER.error("failed to read heroes.json", e)
        }
        return null;
    }
}
