package rtretyak.tlgrm.bots

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.TelegramBotAdapter
import com.pengrad.telegrambot.request.SendMessage
import groovyx.net.http.HttpResponseDecorator

/**
 * Created by tretyak on 6/29/2017.
 */
abstract class Bot {
    protected TelegramBot bot;
    protected ObjectMapper mapper;
    protected List<Long> recipients

    public Bot(String token, recipients) {
        bot = TelegramBotAdapter.build(token)
        this.recipients = recipients
        mapper = initMapper()
    }

    public abstract void start()

    public void sendMessage(String msg) {
        recipients.each{ bot.execute(new SendMessage(it, msg).disableNotification(true)) }
    }

    public static Long convertCommunityIdToSteamId3(long communityId) {
        return communityId - 76561197960265728L
    }

    def initMapper() {
        def mapper = new ObjectMapper()
        mapper.enable(SerializationFeature.INDENT_OUTPUT)
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
        injectJacksonFeatures(mapper)
        return mapper
    }

    static def injectJacksonFeatures(mapper) {
        HttpResponseDecorator.metaClass.asType = { Class clazz ->
            mapper.convertValue(delegate.data, clazz)
        }
        Map.metaClass.to = { Class clazz ->
            mapper.convertValue(delegate, clazz)
        }
        String.metaClass.asType = { Class clazz ->
            mapper.readValue(delegate, clazz)
        }
        Object.metaClass.toJson = { ->
            mapper.writeValueAsString(delegate)
        }
    }
}
