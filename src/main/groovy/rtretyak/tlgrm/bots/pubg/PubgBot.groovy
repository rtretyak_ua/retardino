package rtretyak.tlgrm.bots.pubg

import groovyx.net.http.RESTClient
import rtretyak.tlgrm.bots.Bot
import rtretyak.tlgrm.domain.pubg.PubgPlayer
import rtretyak.tlgrm.domain.pubg.stats.LiveTracking
import rtretyak.tlgrm.domain.pubg.stats.Match
import rtretyak.tlgrm.domain.pubg.stats.Stats
import org.apache.log4j.Logger

/**
 * Created by tretyak on 7/4/2017.
 */
class PubgBot extends Bot {
    private final Logger LOGGER = Logger.getLogger(this.class);
    private final Integer TIMEOUT = 10000

    RESTClient pubgApi
    List<Long> playerIds
    private static final String PUBGTRACKER_URL = "https://pubgtracker.com/"
    private static final def PLAYER_STATS = {nick -> "/api/profile/pc/$nick"}
    private static final String SEARCH_PROFILE = "/api/search"

    PubgBot(String token, String pubgTrackerToken, List<Long> recipients, List<Long> playerIds) {
        super(token, recipients)
        pubgApi = new RESTClient(PUBGTRACKER_URL)
        pubgApi.headers["TRN-Api-Key"] = pubgTrackerToken
        pubgApi.client.params.setParameter('http.connection.timeout', TIMEOUT)
        pubgApi.client.params.setParameter('http.socket.timeout', TIMEOUT)
        this.playerIds = playerIds
    }

    @Override
    void start() {
        sendMessage("Coming online.")
        List<PubgPlayer> players = playerIds.collect {
           pubgApi.get(path: SEARCH_PROFILE, query: [steamId: it]) as PubgPlayer
        }
        players.each { player ->
            Stats statsResponse = pubgApi.get(path: PLAYER_STATS(player.nickname)) as Stats
            player.latestMatch = statsResponse.matchHistory.first()
            player.latestLiveTracking = statsResponse.liveTracking.first()
        }
        while(true) {
            players.each { player ->
                try { checkForUpdates(player) }
                catch(Throwable t) {
                    LOGGER.error("failed to check for updates", t)
                    //do not react, just sleep and retry
                }
            }
            LOGGER.info("sleeping for a minute")
            sleep(60 * 1000)
        }
    }

    void checkForUpdates(PubgPlayer player) {
        Stats statsResponse = pubgApi.get(path: PLAYER_STATS(player.nickname)) as Stats
        Match latestMatch = statsResponse.matchHistory.first()
        LiveTracking latestLiveTracking = statsResponse.liveTracking.first()
        if(player.latestMatch.id != latestMatch.id) {
            player.latestLiveTracking = latestLiveTracking
            player.latestMatch = latestMatch
            def msg = "${player.nickname} (${latestLiveTracking.region} ${latestLiveTracking.matchDisplay})" +
                    " ${latestLiveTracking.message ?: ''}\n" +
                    "${latestMatch.top10 > 0 ? "In Top10" : "Not in Top10"}, Kills: ${latestMatch.kills}, Assists: ${latestMatch.assists}\n" +
                    "New rating: ${latestMatch.rating.round(2)} (${latestMatch.ratingChange > 0 ? '+' : ''}${latestMatch.ratingChange.round(2)}). " +
                    "New rank: ${latestMatch.ratingRank} (${-1*latestMatch.ratingRankChange > 0 ? '+' : ''}${-1*latestMatch.ratingRankChange})"
            sendMessage(msg)
        }
    }
}
