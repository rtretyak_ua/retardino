package rtretyak.tlgrm

import org.apache.log4j.Logger
import rtretyak.tlgrm.bots.dota.Dota2Bot
import rtretyak.tlgrm.bots.pubg.PubgBot

/**
 * Created by tretyak on 6/29/2017.
 */
class Main {
    private final Logger LOGGER = Logger.getLogger(this.class);

    static long mdkId = -248819418;
    static List<Long> pubgPlayerIds = initPubgIds();
    static List<Long> dotaPlayerIds = initDotaIds();
    static Properties props = readPropsFile();

    public static void main(String[] args) throws Exception {
        PubgBot pubgBot = new PubgBot(props.pubgBotToken, props.pubgTrackerApiKey, [mdkId], pubgPlayerIds)
        Dota2Bot d2Bot = new Dota2Bot(props.dotaBotToken, props.steamToken, [mdkId], dotaPlayerIds)
        Thread.start {
            try { pubgBot.start() }
            catch(Throwable t) { LOGGER.error("pubg error", t) }
        }
        Thread.start {
            try { d2Bot.start() }
            catch(Throwable t) { LOGGER.error("dota2 error", t) }
        }
    }

    private static Properties readPropsFile() {
        Properties properties = new Properties()
        File propertiesFile = new File('tokens.properties')
        propertiesFile.withInputStream {
            properties.load(it)
        }
        return properties
    }

    private static List<Long> initDotaIds() {
        return [
                76561198063155492L, //Faka
                76561198011761366L, //Roman
                76561197999630727L, //Yas
                76561198067656948L, //Stas
                76561198075457920L, //Max
                76561198079779639L, //Haruna
        ]
    }

    private static List<Long> initPubgIds() {
        return [
                76561198079779639L, //Haruna
        ]
    }
}
